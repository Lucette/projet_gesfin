﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GesFin.Models
{
    public class Dal : IDal
    {
        private BddContext bdd;

        public Dal()
        {
            bdd = new BddContext();
        }

        public List<Transactions> ListTransactions()
        {
            return bdd.Transactions.ToList();
        }

        public void Dispose()
        {
            bdd.Dispose();
        }

    }
}