﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GesFin.Models
{
    [Table("Compte")]
    public class Compte
    {
        [Key()]
        public int Id { get; set; }
        public string Iban { get; set; }
        public string Banque { get; set; }
        public double Solde { get; set; }
        public double Budget { get; set; }
        public string Type { get; set; }
        public virtual Client Client { get; set; }

    }
}