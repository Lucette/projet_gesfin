﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GesFin.Models
{
    public class Budget
    {
        public int Id { get; set; }
        public double Montant { get; set; }
        public string Mois { get; set; }
        public Categorie Categorie { get; set; }
        public Compte Compte { get; set; }
    }
}