﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GesFin.Models
{
    public class Transactions
    {
        public int Id { get; set; }
        public string Motif { get; set; }
        public double Montant { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public Categorie Categorie { get; set; }
        public Compte Compte { get; set; }
    }
}