﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GesFin.Models
{
    public interface IDal : IDisposable
    {
        void AjouterTransaction(string motif, double montant, string type, DateTime date, Categorie categorie, Compte compte);
        List<Transactions> ListTransactions();
    }
}
