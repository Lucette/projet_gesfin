﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GesFin.Models
{
    public class BddContext : DbContext
    {
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Compte> Compte { get; set; }
    }
}